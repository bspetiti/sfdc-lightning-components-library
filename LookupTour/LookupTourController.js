({
    doInit: function(component,event,helper){
        var searchFieldId = component.get('v.selectRecordId');
        var searchFieldValue = component.get('v.selectRecordName');
        component.set('v.currentSelectRecordId',searchFieldId);
        component.set('v.currentSelectRecordName',searchFieldValue);
    },
    search: function (component, event, helper) {
        var searchFieldValue = component.get('v.selectRecordName');
        console.log('Current value1: '+component.get('v.selectRecordName'));
        console.log('Current value2: '+component.get('v.currentSelectRecordName'));
        var resultBox = component.find('resultBox');
        var enterKeyCode = 13;
        var downKeyCode = 40;
        var upKeyCode = 38;
        var keyPressed = event.which;
        var specialCharacters = component.get('v.specialCharacters');
        if (keyPressed == enterKeyCode) {
            helper.populateSearchFieldWithSelectedResult(component, event, helper);
        } else if (keyPressed == downKeyCode) {
            helper.selectNextResultValue(component, keyPressed, helper);
        } else if (keyPressed == upKeyCode) {
            helper.selectPreviousResultValue(component, keyPressed, helper);
        } else if(searchFieldValue.length>0 && !specialCharacters.includes(keyPressed)){
            helper.executeSearchAndLoadResults(component, keyPressed, searchFieldValue);

        }
        if(searchFieldValue.length==0){
            helper.cleanDataFromLists(component,resultBox,helper);
        }
    },

    setSelectedRecord: function (component, event, helper) {
        var currentText = event.currentTarget.id;
        var resultBox = component.find('resultBox');
        $A.util.removeClass(resultBox, 'slds-is-open');
        component.set("v.selectRecordName", event.currentTarget.dataset.name);
        component.set("v.selectRecordId", currentText);
    },

})