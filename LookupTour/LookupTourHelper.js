({
    populateSearchFieldWithSelectedResult: function (component, event, helper) {
        var iterationIndex = component.get('v.iterationIndex');
        var searchResults = component.get('v.searchRecords');
        var resultBox = component.find('resultBox');
        if (searchResults.length > 0) {
            component.set("v.selectRecordName", searchResults[iterationIndex].recName);
            component.set("v.selectRecordId", searchResults[iterationIndex].recId);
        }

        $A.util.removeClass(resultBox, 'slds-is-open');
        component.set('v.iterationIndex', 0);
        component.set('v.searchRecords', []);
    },

    selectNextResultValue: function (component, resultBox, helper) {
        var iterationIndex = component.get('v.iterationIndex');
        var searchResults = component.get('v.searchRecords');
        if (searchResults.length > 0 && iterationIndex < searchResults.length - 1) {
            iterationIndex += 1;
            component.set("v.selectRecordName", searchResults[iterationIndex].recName);
            component.set("v.selectRecordId", searchResults[iterationIndex].recId);
            component.set("v.iterationIndex", iterationIndex);
        }
    },


    selectPreviousResultValue: function (component, resultBox, helper) {
        var iterationIndex = component.get('v.iterationIndex');
        var searchResults = component.get('v.searchRecords');
        if (searchResults.length > 0 && iterationIndex > 0) {
            iterationIndex -= 1;
            component.set("v.selectRecordName", searchResults[iterationIndex].recName);
            component.set("v.selectRecordId", searchResults[iterationIndex].recId);
            component.set("v.iterationIndex", iterationIndex);
        }
    },

    cleanDataFromLists: function (component, resultBox, helper) {
        component.set('v.iterationIndex', 0);
        component.set('v.searchRecords', []);
        component.set('v.keyPressCode', 0);
        var currentId = component.get('v.currentSelectRecordId');
        component.set('v.selectRecordId',currentId);
        $A.util.removeClass(resultBox, 'slds-is-open');
    },

    populateSearchFieldWithExactWroteText: function(component, event, helper){
        var searchFieldValue = component.get('v.selectRecordName');
        var searchResults = component.get('v.searchRecords');
        var resultBox = component.find('resultBox');
        console.log('search value: '+searchFieldValue);
        console.log('searchResults: '+searchResults.length);
        if (searchResults.length > 0 && searchResults[0].recName.includes(searchFieldValue) && searchResults.length == 1) {
            component.set("v.selectRecordName", searchResults[0].recName);
            component.set("v.selectRecordId", searchResults[0].recId);
            $A.util.removeClass(resultBox, 'slds-is-open');
            component.set('v.searchRecords', []);
        }
    },

    executeSearchAndLoadResults: function (component, keyPressed, helper) {
        var resultBox = component.find('resultBox');
        var backspaceKeyCode = 8;
        var currentText = component.get('v.selectRecordName');
        component.set("v.LoadingText", true);
        if (currentText.length > 0) {
            $A.util.addClass(resultBox, 'slds-is-open');
        } else {
            $A.util.removeClass(resultBox, 'slds-is-open');
        }
        var action = component.get("c.getResults");
        action.setParams({
            "ObjectName": component.get("v.objectName"),
            "fieldName": component.get("v.fieldName"),
            "fieldName2": component.get("v.fieldName2"),
            "fieldName3": component.get("v.fieldName3"),
            "field2Value": component.get('v.field2Value'),
            "field3Value": component.get('v.field3Value'),
            "value": currentText
        });

        action.setCallback(this, function (response) {
            var STATE = response.getState();
            if (STATE === "SUCCESS") {
                component.set("v.searchRecords", response.getReturnValue());
                if (component.get("v.searchRecords").length == 0) {
                    console.log('000000');
                    var currentId = component.get('v.currentSelectRecordId');
                    component.set('v.selectRecordId',currentId);
                } else {
                    if (keyPressed != backspaceKeyCode) this.populateSearchFieldWithExactWroteText(component, event, helper);
                }

            } else if (STATE === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " +
                            errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.LoadingText", false);
        });
        $A.enqueueAction(action);
    }
})